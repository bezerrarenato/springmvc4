package br.com.flapanq.painel.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DaoSupport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.flapanq.contas.modelo.Noticia;
import br.com.flapanq.contas.modelo.dao.NoticiaDao;
import br.com.flapanq.painel.constants.FileConstants;

@RestController
public class GerenciarImagemController {
	
	private NoticiaDao noticiaDao;
	
	@Autowired
	public GerenciarImagemController(NoticiaDao noticiaDao) {
       this.noticiaDao = noticiaDao;
	}
	
	@Autowired
    private ServletContext context; 
	
	@RequestMapping("lista-imagens")
	public List<String> listaImagens(@RequestParam("id") Long id){
		return noticiaDao.listarImagens(id);
	}
	
	@RequestMapping("exclui-imagem")
	public void excluiImagem(@RequestParam("id") Long id, @RequestParam("nomeimg") String nomeimg){
		noticiaDao.removerImagem(id, nomeimg);
	}
	
	
	@RequestMapping("cadastrar-imagem")
	public String cadastrarImagem(@RequestParam("id") Long id,  @RequestParam("file") MultipartFile file){
		
		String filePath = context.getRealPath(File.separator +"upload"+File.separator);
		
		StringBuilder stb = new StringBuilder();
      //  stb.append(FileConstants.FILE_PATH);
		stb.append(filePath);
        stb.append(File.separator);
		stb.append(file.getOriginalFilename());
		
		noticiaDao.salvarImagem(id, file.getOriginalFilename());
		
		if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File(stb.toString())));
                stream.write(bytes);
                stream.close();
                return "You successfully uploaded " +  file.getName() + "!";
            } catch (Exception e) {
                return "You failed to upload " + file.getName() + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + "arquivo" + " because the file was empty.";
        }
		
		
	}
	
	
	
	
	
	
}
