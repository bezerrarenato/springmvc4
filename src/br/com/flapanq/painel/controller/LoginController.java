package br.com.flapanq.painel.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.flapanq.contas.ajax.LoginResponseBody;
import br.com.flapanq.contas.ajax.UserPainel;

@RestController
public class LoginController {

	@RequestMapping(value = "/logarpainel")
	public LoginResponseBody logar(@RequestBody UserPainel userPainel) {

		LoginResponseBody loginResponseBody = new LoginResponseBody();

		if (userPainel.getSenha().equals("1")
				&& userPainel.getUsuario().equals("1")) {
			loginResponseBody.setLogado(true);
		} else {
			loginResponseBody.setLogado(false);	
		}

		return loginResponseBody;

	}

}
