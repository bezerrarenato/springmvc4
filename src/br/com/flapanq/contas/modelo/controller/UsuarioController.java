package br.com.flapanq.contas.modelo.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.flapanq.contas.modelo.Usuario;
import br.com.flapanq.contas.modelo.dao.UsuarioDAO;

@Controller
public class UsuarioController {

	private UsuarioDAO usuarioDAO;
	
	@RequestMapping(value = "/seachuser")
	public String searchUser(){
		return "search-usuario";
	}

	@Autowired
	public UsuarioController(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	@RequestMapping("/loginForm")
	public ModelAndView formulario() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("usuario/login");
		return modelAndView;
	}

	// somente reconhecer� o acesso � URL de autentica��o se
	// o m�todo HTTP for um POST
	@RequestMapping(value = "/efetuaLogin", method = RequestMethod.POST)
	// @RequestMapping("/efetuaLogin" )
	// acessa as propriedades do usuario automaticamente
	public String efetuaLogin(@Valid Usuario usuario,BindingResult bindingResult, HttpSession httpSession) {
		// declaramos qual parametros queremos receber
		// public String efetuaLogin(@RequestParam("login") String
		// login,@RequestParam("senha") String senha, HttpSession httpSession){

		// Usuario usuario = new Usuario();
		// usuario.setLogin(login);
		// usuario.setSenha(senha);
		boolean existeUsuario = usuarioDAO.existeUsuario(usuario);

		if (existeUsuario) {
			httpSession.setAttribute("usuarioLogado", usuario);
			return "usuario/bemvindo";
		} else {
			return "usuario/login";
		}

	}

}
