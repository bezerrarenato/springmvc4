package br.com.flapanq.contas.modelo.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import br.com.flapanq.contas.modelo.Conta;
import br.com.flapanq.contas.modelo.TipoDaConta;
@Component
public class ContaDAO {

	
	private static Map<Integer,Conta> listaConta = new HashMap<Integer,Conta>();
	
	static {
		
		Conta contaLuz = new Conta(1, "LUZ", true, 10.00, Calendar.getInstance(), TipoDaConta.ENTRADA);

		Conta contaAgua = new Conta(2, "LUZ", false, 10.00, Calendar.getInstance(), TipoDaConta.ENTRADA);
		
		listaConta.put(1,contaLuz);
		
		listaConta.put(2,contaAgua);
		
	}
	
	public ContaDAO() {

	}

	public void adiciona(Conta conta) {
		Integer id = listaConta.size()+ 1;
		conta.setId(id);
		listaConta.put(id,conta);
	}

	public void remove(Conta conta) {
        listaConta.remove(conta.getId());
	}

	public void altera(Conta conta) {
	  Conta contaEncontrada = listaConta.get(conta.getId());
	  contaEncontrada = conta;
	}

	public List<Conta> lista() {
		return new ArrayList<Conta>(listaConta.values());
	}

	public Conta buscaPorId(Integer id) {
        return listaConta.get(id);
	}

	public void paga(Integer id) {
       listaConta.get(id).setPaga(true);
	}

}
