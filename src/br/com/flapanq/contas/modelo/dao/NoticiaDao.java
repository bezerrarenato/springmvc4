package br.com.flapanq.contas.modelo.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.flapanq.contas.modelo.Noticia;

@Component
public class NoticiaDao {

	private static List<Noticia> listaNoticias = new ArrayList<Noticia>();

	static {

		Noticia noticiaManha = new Noticia();
		noticiaManha.setId(1l);
		noticiaManha.setStatus(1);
		noticiaManha
				.setTexto("texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto");
		noticiaManha
				.setTitulo("Titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo ");
		noticiaManha
				.setDescricao("descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o");
		noticiaManha.setData(new Date());

		Noticia noticiaTarde = new Noticia();
		noticiaTarde.setId(2l);
		noticiaTarde.setStatus(1);
		noticiaTarde
				.setTexto("texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto");
		noticiaTarde
				.setTitulo("Titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo ");
		noticiaTarde
				.setDescricao("descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o");
		noticiaTarde.setData(new Date());

		Noticia noticiaNoite = new Noticia();
		noticiaNoite.setId(3l);
		noticiaNoite.setStatus(1);
		noticiaNoite
				.setTexto("texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto");
		noticiaNoite
				.setTitulo("Titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo titulo ");
		noticiaNoite
				.setDescricao("descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o descri��o");
		noticiaNoite.setData(new Date());

		listaNoticias.add(noticiaManha);
		listaNoticias.add(noticiaTarde);
		listaNoticias.add(noticiaNoite);

	}

	public List<Noticia> listarTodas() {
		return listaNoticias;
	}

	public void cadastrar(Noticia noticia) {
		listaNoticias.add(noticia);
	}

	public void removerImagem(Long id, String imagem) {

		for (Noticia not : listaNoticias) {
			if (not.getId().equals(id)) {
				not.getListaImagem().remove(imagem);
//				for (int i = 0; i < not.getListaImagem().toArray().length; i++) {
//					if (not.getListaImagem().toArray()[i].equals(imagem)) {
//						not.getListaImagem().toArray()[i] = null;
//						break;
//					}
//				}

			}
		}

	}

	public void salvarImagem(Long id, String imagem) {

		for (Noticia not : listaNoticias) {
			if (not.getId().equals(id)) {
				not.getListaImagem().add(imagem);
			}
		}
	}

	@RequestMapping("id")
	public List<String> listarImagens(Long id) {
		for (Noticia not : listaNoticias) {
			if (not.getId().equals(id)) {
				return not.getListaImagem();
			}
		}
		return null;
	}

}
