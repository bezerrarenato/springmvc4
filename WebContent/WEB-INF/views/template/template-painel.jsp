<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html >
<head>
<title> Painel Administrativo - Login  </title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<link href="resources/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">
<link href="resources/css/angular/loading-bar.min.css" rel="stylesheet">
<link href="resources/css/jquery/jquery.gritter.css" rel="stylesheet">
</head>
<body>

    <!-- angular -->
    <script type="text/javascript" src="resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="resources/js/angular/ui-utils.min.js" ></script>
    <script type="text/javascript" src="resources/js/angular/loading-bar.min.js" ></script>
    <script type="text/javascript" src="resources/js/angular/angular-file-upload.min.js" ></script>
    
    <!-- jquery -->
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="resources/js/jquery/jquery.gritter.min.js" ></script>
    
	<tiles:insertAttribute name="conteudo" />

    
    
</body>
</html>