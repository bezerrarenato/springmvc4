var app = angular.module("app", [ 'angular-loading-bar', 'angularFileUpload' ]);

app.controller('GerenciarImagensController', function($scope, $http, $location,
		FileUploader) {

	$scope.getNoticia = function() {
		$http.get('/SpringMVC4/views/get-noticia', {
			params : {
				id : $location.search().id
			}
		}).success(function(data) {
			$scope.noticia = data;
			console.log(data);
		}).error(function() {
			alert("Falha na aplicação");
		});
	};
	
	$scope.listaImagens = [];
	
	$scope.getImagens = function() {
		$http.get('/SpringMVC4/views/lista-imagens', {
			params : {
				id : $location.search().id
			}
		}).success(function(data) {
			$scope.listaImagens = data;
			console.log(data);
		}).error(function() {
			alert("Falha na aplicação");
		});
	};
	
	$scope.excluiImagens = function(nomeimg) {
		
		if(!confirm("Tem certeza que deseja excluir ?")) return false;
		
		$http.get('/SpringMVC4/views/exclui-imagem', {
			params : {
				id : $location.search().id , nomeimg : nomeimg 
			}
		
		}).success(function(data) {
			$scope.getImagens();	
		}).error(function() {
			alert("Falha na aplicação");
		});
	};
	
	$scope.getImagens();

	$scope.getNoticia();

	// upload
	var uploader = $scope.uploader = new FileUploader({
		url : '/SpringMVC4/views/cadastrar-imagem?id='+ $location.search().id
	});

	uploader.filters.push({
		name : "tamanhoFila",
		fn : function(item, options) {
			return this.queue.length < 4;
		}
	});

	uploader.onSuccessItem = function(fileItem) {
		console.log("Item enviado com sucesso!");
		$scope.getImagens();
		fileItem.remove();
	};

	uploader.onWhenAddingFileFailed = function(fileItem) {
		console.info("Erro ao adicionar elemento", fileItem);
	};
	
	uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

	// fim upload

});

// configuracao do location que recupera paramentros da url
app.config(function($locationProvider) {
	$locationProvider.html5Mode({
		enabled : true,
		requireBase : false

	});
});
