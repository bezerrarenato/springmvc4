var app = angular.module("app", []);

app.controller('NoticiaController', function($scope, $http) {

	 $scope.getNoticias = function(){
	        $scope.noticias = {};
	        $http.get('/SpringMVC4/views/listar-noticia')
	            .success(function(data){
	                $scope.noticias = data;
	            })
	            .error(function(){
	                alert("Falha em obter notícia");
	            });
	    };
	    $scope.getNoticias();
});
